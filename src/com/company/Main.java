package com.company;

public class Main {

    public static void main(String[] args) {
        Stack newStack = new Stack();
        newStack.push("Pierwszy");
        newStack.push("Drugi");
        newStack.push("Trzeci");

        System.out.println(newStack.get());
        newStack.pop();
        System.out.println(newStack.get());
        newStack.pop();
        System.out.println(newStack.get());
        newStack.pop();
    }
}
