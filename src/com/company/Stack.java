package com.company;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Stack implements StackOperations {
    List<String> stack = new ArrayList<>();

    public List<String> get() {
        List<String> stackTemp = new ArrayList<>();
        stackTemp.addAll(stack);

        Collections.reverse(stackTemp);
        return stackTemp;
    }

    public Optional<String> pop() {
        if (!stack.isEmpty()) {
            String popTemp = stack.remove(stack.size() - 1);
            return Optional.of(popTemp);
        }
        return Optional.empty();
    }

    public void push(String item) {
        stack.add(item);
    }
}
